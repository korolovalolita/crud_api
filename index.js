import dotenv from "dotenv";
import express from "express";
import jsonwebtoken from "jsonwebtoken";
import mongoose from "mongoose";
import routes from "./src/routers/appRouter.js";

dotenv.config();

const URL = process.env.MONGOLAB_URI;
const PORT = process.env.PORT;
const KEY = process.env.PASSWORD_KEY;

const app = express();
mongoose.connect(URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use((req, res, next) => {
  if (
    req.headers &&
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "JWT"
  ) {
    jsonwebtoken.verify(
      req.headers.authorization.split(" ")[1],
      KEY,
      (err, decode) => {
        if (err) req.user = undefined;
        req.user = decode;
        next();
      }
    );
  } else {
    req.user = undefined;
    next();
  }
});

routes(app);

app.get("/", (req, res) => {
  res.send(`Server running on port ${PORT}`);
});

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
