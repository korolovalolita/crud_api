import { createUser, loginUser, loginRequired } from "../controllers/auth.js";
import { changePassword, deleteUser, getUser } from "../controllers/me.js";
import {
  createNote,
  getNotes,
  getNoteById,
  deleteNoteById,
  checkUncheckNote,
  editNote,
} from "../controllers/notes.js";

const routes = (app) => {
  app.route("/api/auth/register").post(createUser);

  app.route("/api/auth/login").post(loginUser);

  app
    .route("/api/users/me")
    .get(loginRequired, getUser)
    .delete(loginRequired, deleteUser)
    .patch(loginRequired, changePassword);

  app
    .route("/api/notes")
    .get(loginRequired, getNotes)
    .post(loginRequired, createNote);

  app
    .route("/api/notes/:id")
    .get(loginRequired, getNoteById)
    .put(loginRequired, editNote)
    .patch(loginRequired, checkUncheckNote)
    .delete(loginRequired, deleteNoteById);
};

export default routes;
