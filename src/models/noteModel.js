import mongoose from "mongoose";

const Schema = mongoose.Schema;

export const NoteSchema = new Schema({
  userID: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    required: true,
    default: false
  },
  text: {
    type: String,
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now
  }
})