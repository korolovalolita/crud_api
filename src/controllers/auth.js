import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import { UserSchema } from "../models/userModel.js";

const User = mongoose.model("User", UserSchema);

export const loginRequired = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    return res.status(400).json({ message: "Unauthorized user" });
  }
};

export const createUser = (req, res) => {
  if (req.body.username) {
    if (req.body.password) {
      User.findOne({ username: req.body.username }, (err, existingUser) => {
        if (err) {
          console.error(err);
        }
        if (!existingUser) {
          let user = new User(req.body);
          user.hashPassword = bcrypt.hashSync(req.body.password, 10);

          user.save((err, newUser) => {
            if (err) {
              res.status(400).json({ message: err });
            } else {
              newUser.hashPassword = undefined;
              return res.json({ message: "Success" });
            }
          });
        } else {
          res.status(400).json({ message: "User already exists" });
        }
      });
    } else {
      res.status(400).json({ message: "Specify password" });
    }
  } else {
    res.status(400).json({ message: "Specify username" });
  }
};

export const loginUser = (req, res) => {
  User.findOne({ username: req.body.username }, (err, user) => {
    if (err) {
      res.status(500).json({ message: "Internal Server Error" });
    } else {
      if (user) {
        if (user.comparePassword(req.body.password, user.hashPassword)) {
          res.status(200).json({
            message: "Success",
            jwt_token: jwt.sign(
              { _id: user._id,
                username: user.username
              },
              "HW2"
            ),
          });
        } else {
          res.status(400).json({ message: "Wrong password" });
        }
      } else {
        res.status(400).json({ message: "User not found" });
      }
    }
  });
};
