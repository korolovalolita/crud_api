import mongoose from "mongoose";
import { NoteSchema } from "../models/noteModel.js";

const Note = mongoose.model("Note", NoteSchema);

export const createNote = (req, res) => {
  if (req.body.text) {
    let note = new Note(req.body);
    note.userID = req.user._id;

    note.save((err) => {
      if (err) {
        res.status(500).json({ message: "Internal Server Error" });
      } else {
        res.json({ message: "Success" });
      }
    });
  } else {
    res.status(400).json({ message: "Specify note text" });
  }
};

export const getNotes = (req, res) => {
  Note.find({ userID: req.user }, (err, notes) => {
    if (err) {
      res.status(500).json({ message: "Internal Server Error" });
    } else {
      res.json({
        offset: req.body.offset || 0,
        limit: req.body.limit || 0,
        count: notes.length - req.body.offset || 0,
        notes: notes,
      });
    }
  })
    .skip(req.body.offset || 0)
    .limit(req.body.limit || 0);
};

export const getNoteById = (req, res) => {
  Note.findById(req.params.id, (err, note) => {
    if (err) {
      res.status(500).json({ message: "Internal Server error" });
    } else {
      if (!note) {
        res.status(400).json({ message: "There is no such note" });
      } else {
        res.json({
          note: {
            _id: note._id,
            userID: note.userID,
            completed: note.completed,
            text: note.text,
            createdDate: note.createdDate,
          },
        });
      }
    }
  });
};

export const deleteNoteById = (req, res) => {
  Note.findByIdAndRemove(req.params.id, (err, note) => {
    if (err) {
      res.status(500).json({ message: "Internal Server error" });
    } else {
      if (!note) {
        res.status(400).json({ message: "There is no such note" });
      } else {
        res.json({ message: "Success" });
      }
    }
  });
};

export const checkUncheckNote = (req, res) => {
  Note.findById(req.params.id, (err, note) => {
    if (err) {
      res.status(500).json({ message: "Internal Server error" });
    } else {
      if (!note) {
        res.status(400).json({ message: "There is no such note" });
      } else {
        if (note.completed === false) {
          note.completed = true;
        } else if (note.completed === true) {
          note.completed = false;
        }
        note.save();
        res.json({ message: "Success" });
      }
    }
  });
};

export const editNote = (req, res) => {
  Note.findById(req.params.id, (err, note) => {
    if (err) {
      res.status(500).json({ message: "Internal Server error" });
    } else {
      if (!note) {
        res.status(400).json({ message: "There is no such note" });
      } else {
        if (req.body.text) {
          note.text = req.body.text;
          note.save();
          res.json({ message: "Success" });
        } else {
          res.status(400).json({ message: "Enter new text to the note"});
        }
      }
    }
  });
};
